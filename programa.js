var contexto; //define una variable global o local en una función sin importar el ámbito del bloque.

function inicializar() {
    let lienzo = document.getElementById('miLienzo'); //llamar canva mi lienzo.
    lienzo.width = window.innerWidth;// definir ancho
    lienzo.height = window.innerHeight; // definir la altura
    contexto = lienzo.getContext('2d'); //graficas tipo 2D
    contexto.moveTo(0, (window.innerHeight / 2) - 100);//formular la linea 
    contexto.translate(0, window.innerHeight/2);// Definir una posicion 
    contexto.scale(1, -1);//modifica el tamaño de un elemento en el plano 2D.
    
    contexto.moveTo(0, (window.innerHeight / 2) - 210);
    contexto.lineTo(window.innerWidth, (window.innerHeight / 2) - 210);
    contexto.stroke();
    
}





function dibujarCirculo(x, y, radio) {
    contexto.beginPath(); //comienza una ruta o restablece la ruta actual.
    contexto.arc(x, y, radio, 0, 2 * Math.PI, true); //crear un arco
    contexto.fill(); //Matriz
}


function funcionSeno(frecuencia) {
    inicializar()
    let x;
    let desplazamiento = 100;
    let amplitud = 100;
    for (x = 0; x < 360 * 4; x += (1 / frecuencia)){
        y = Math.sin(x * frecuencia * Math.PI / 180) * amplitud + desplazamiento;
        dibujarCirculo(x, y, 1);
    }
  


}

function funcionCoseno(frecuencia) {
    inicializar()
    let x;
    let desplazamiento = 100;
    let amplitud = 100;
    for (x = 0; x < 360 * 4; x += (1 / frecuencia)){
        y = Math.cos(x * frecuencia * Math.PI / 180) * amplitud + desplazamiento;
        dibujarCirculo(x, y, 1)
    }

}